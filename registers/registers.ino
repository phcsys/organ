#include "Wire.h"
#include <Adafruit_MCP23017.h>

Adafruit_MCP23017 mcp1;
Adafruit_MCP23017 mcp2;
Adafruit_MCP23017 mcp3;

#define CHANNEL 3 //MIDI channels 1-16 are really 0-15

boolean state;

int maunal_state[3][16] = {
  {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
  {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
  {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}
};

const int manual_digit[16] = { 0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15 };

const int manual[3][16] = {
  { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16 },
  { 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32 },
  { 33, 34, 35, 36, 37, 38, 39, 40, 41, 42, 43, 44, 45, 46, 47, 48 }
};

void setup()
{
  while (!Serial) { delay(10); }
  Serial.begin(31250); //31250

  mcp1.begin(0);
  mcp2.begin(1);
  mcp3.begin(2);

  delay(20);

  read_states_by_start(0);
  read_states_by_start(1);
  read_states_by_start(2);
}

void loop()
{
  read_states(0);
  read_states(1);
  read_states(2);

  midiThru();
}

void read_states(int pos) {
  for (int i=0; i<sizeof manual_digit/sizeof manual_digit[0]; i++) {
    state = read_value(pos, manual_digit[i]);

    if(!state) {
      if(maunal_state[pos][i] != 1) {
        playMIDINote(manual[pos][i], 127);
        maunal_state[pos][i] = 1;
      }
    }
    else {
      if(maunal_state[pos][i] == 1) {
        maunal_state[pos][i] = 0;
        playMIDINote(manual[pos][i], 0);
      }
    }
  }
}

void read_states_by_start(int pos) {
  for (int i=0; i<sizeof manual_digit/sizeof manual_digit[0]; i++) {
    state = read_value(pos, manual_digit[i]);

    if(!state) {
      playMIDINote(manual[pos][i], 127);
      maunal_state[pos][i] = 1;
    }
    else {
      maunal_state[pos][i] = 0;
      playMIDINote(manual[pos][i], 0);
    }
  }
}

bool read_value(int pos, int digit) {
  switch (pos) {
    case 0:
      state = mcp1.digitalRead(digit);
      break;
    case 1:
      state = mcp2.digitalRead(digit);
      break;
    case 2:
      state = mcp3.digitalRead(digit);
      break;
    default:
      state = false;
  }

  return state;
}

void midiThru() {
  if(Serial.available() >= 3) {

      Serial.write(Serial.read());
    }
  }

  if(Serial.available() > 3) {
    //Serial.flush();
  }
}

void playMIDINote(byte note, byte velocity)
{
    if(velocity > 0x00) {
      byte noteOnStatus = 0x90 + CHANNEL;
      Serial.write(noteOnStatus);
      Serial.write(note);
      Serial.write(velocity);
    }
    else {
      byte noteOnStatus = 0x80 + CHANNEL;
      Serial.write(noteOnStatus);
      Serial.write(note);
      Serial.write(velocity);
    }
}
