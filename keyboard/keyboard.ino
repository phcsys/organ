#include "Wire.h"
#include <Adafruit_MCP23017.h>

Adafruit_MCP23017 mcp1;
Adafruit_MCP23017 mcp2;
Adafruit_MCP23017 mcp3;
Adafruit_MCP23017 mcp4;
Adafruit_MCP23017 mcp5;

#define CHANNEL 0 //MIDI channels 1-16 are really 0-15

byte midiIn[3];

boolean state;
int maunal_state_master[13] = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};

const byte noteOn = 0x90 + CHANNEL;
const byte noteOff = 0x80 + CHANNEL;

//const int manual_master[13] = { 68, 69, 70, 71, 72, 60, 61, 62, 63, 64, 65, 66, 67 };

const int manual_digit_master[13] = { 8, 9, 10, 11, 12, 13, 14, 15, 4, 3, 2, 1, 0 };
const int manual_master[13] = { 84, 85, 86, 87, 88, 89, 90, 91, 92, 93, 94, 95, 96 };


int maunal_state[4][12] = {
  {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
  {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
  {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
  {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}
};

const int manual_digit[12] = { 8, 9, 10, 11, 12, 13, 14, 15, 3, 2, 1, 0 };

const int manual[4][12] = {

  { 72, 73, 74, 75, 76, 77, 78, 79, 80, 81, 82, 83 },
  { 60, 61, 62, 63, 64, 65, 66, 67, 68, 69, 70, 71 },
  { 48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 58, 59 },
  { 36, 37, 38, 39, 40, 41, 42, 43, 44, 45, 46, 47 }
};

void setup()
{
  while (!Serial) { delay(10); }
  Serial.begin(31250); //31250

  mcp1.begin(0);
  mcp2.begin(1);
  mcp3.begin(2);
  mcp4.begin(3);
  mcp5.begin(4);
}

void loop()
{
  read_master_states();
  midiThru();
  read_states(0);
  midiThru();
  read_states(1);
  midiThru();
  read_states(2);
  midiThru();
  read_states(3);
  midiThru();
}

void read_master_states() {
  for (int i=0; i<sizeof manual_digit_master/sizeof manual_digit_master[0]; i++) {
    if(maunal_state_master[i] != 1) {
      if(!mcp1.digitalRead(manual_digit_master[i])) {
        playMIDINote(manual_master[i], 127);
        maunal_state_master[i] = 1;
      }
    }
    else {
      if(mcp1.digitalRead(manual_digit_master[i])) {
        playMIDINote(manual_master[i], 0);
        maunal_state_master[i] = 0;
      }
    }
  }
}


void read_states(int pos) {
  for (int i=0; i<sizeof manual_digit/sizeof manual_digit[0]; i++) {
    state = read_value(pos, manual_digit[i]);

    if(maunal_state[pos][i] != 1) {
      if(!state) {
        playMIDINote(manual[pos][i], 127);
        maunal_state[pos][i] = 1;
      }
    }
    else {
      if(state) {
        playMIDINote(manual[pos][i], 0);
        maunal_state[pos][i] = 0;
      }
    }
  }
}

void midiThru() {
  while(Serial.available() >= 3) {
    for(int a = 0; a < 3; a++) {
      Serial.write(Serial.read());
    }
  }
}

bool read_value(int pos, int digit) {
  switch (pos) {
    case 0:
      state = mcp2.digitalRead(digit);
      break;
    case 1:
      state = mcp3.digitalRead(digit);
      break;
    case 2:
      state = mcp4.digitalRead(digit);
      break;
    case 3:
      state = mcp5.digitalRead(digit);
      break;
    default:
      state = false;
  }

  return state;
}

void playMIDINote(byte note, byte velocity)
{
  if(velocity > 0x00) {
    Serial.write(noteOn);
    Serial.write(note);
    Serial.write(velocity);
  }
  else {
    Serial.write(noteOff);
    Serial.write(note);
    Serial.write(velocity);
  }
}
