#include "Wire.h"
#include <Adafruit_MCP23017.h>

Adafruit_MCP23017 mcp1;
Adafruit_MCP23017 mcp2;

#define CHANNEL 2 //MIDI channels 1-16 are really 0-15

bool state;

int maunal_state[4][16] = {
  {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
  {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}
};

const int manual_digit[16] = { 0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15 };

const int manual[2][16] = {
  { 36, 37, 38, 39, 40, 41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51 },
  { 52, 53, 54, 55, 56, 57, 58, 59, 60, 61, 62, 63, 64, 65, 66, 67 }
};

void setup()
{
  while (!Serial) { delay(10); }
  Serial.begin(31250); //31250

  mcp1.begin(0);
  mcp2.begin(1);
}

void loop()
{
  read_states(0);
  read_states(1);
}

void read_states(int pos) {
  for (int i=0; i<sizeof manual_digit/sizeof manual_digit[0]; i++) {
    state = read_value(pos, manual_digit[i]);

    if(!state) {
      if(maunal_state[pos][i] != 1) {
        playMIDINote(manual[pos][i], 127);
        maunal_state[pos][i] = 1;
      }
    }
    else {
      if(maunal_state[pos][i] == 1) {
        maunal_state[pos][i] = 0;
        playMIDINote(manual[pos][i], 0);
      }
    }
  }
}

bool read_value(int pos, int digit) {
  switch (pos) {
    case 0:
      state = mcp1.digitalRead(digit);
      break;
    case 1:
      state = mcp2.digitalRead(digit);
      break;
    default:
      state = false;
  }

  return state;
}

void midiThru() {
  if(Serial.available() == 3) {
    for(int a = 0; a < 3; a++) {
      Serial.write(Serial.read());
    }
  }

  if(Serial.available() > 3) {
    Serial.flush();
  }
}

void playMIDINote(byte note, byte velocity)
{
  midiThru();

  if(velocity > 0x00) {
    byte noteOnStatus = 0x90 + CHANNEL;
    Serial.write(noteOnStatus);
    Serial.write(note);
    Serial.write(velocity);
  }
  else {
    byte noteOnStatus = 0x80 + CHANNEL;
    Serial.write(noteOnStatus);
    Serial.write(note);
    Serial.write(velocity);
  }
}
