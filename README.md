# MIDI Organ
#### Replace kit for Omegan 8100

_README in Progress_

![Manuals](readme_parts/manuals.png)

![Registers](readme_parts/3d_main.png)


## Features

## Hardware parts
  - MCP23017
  - Arduino Nano v3
  - 4N35
  - 1N914
  - R 220Ω
  - R 10KΩ
  - Resistor Network 10KΩ x 8 (SIP9)
  - JST XH 4P
  - JST XH 3P
  - JST XH 2P
  - PinHeader 2.54mm 1x14
  - PinHeader 2.54mm 1x12
  - PinHeader 2.54mm 1x9


## TODO
  - MIDI in Features
  - Swell Pedal
  - Crescendo Pedal
  - Resend Switch states by start
  - Finish README

## License

[![Creative Commons](readme_parts/cc_blue.svg)
![Attribution](readme_parts/attribution_icon_blue.svg)
![Non Commercial](readme_parts/nc_blue.svg)](https://creativecommons.org/licenses/by-nc/4.0)
